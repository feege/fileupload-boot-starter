package com.gaofei.config;

import com.gaofei.domain.FileProperties;
import com.gaofei.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author : gaofee
 * @date : 13:37 2021/4/23
 * @码云地址 : https://gitee.com/itgaofee
 */
@Configuration
@EnableConfigurationProperties(FileProperties.class)
public class FileAutoConfiguration {


    @Autowired
    private FileProperties fileProperties;

    @Bean
    @ConditionalOnProperty(prefix = "file",value = "enable",havingValue = "true")
    public FileService fileService(){
        return new FileService(fileProperties);
    }

}
