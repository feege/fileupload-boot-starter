package com.gaofei.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author : gaofee
 * @date : 13:25 2021/4/23
 * @码云地址 : https://gitee.com/itgaofee
 */

@Data
@AllArgsConstructor
public class FileResult {

    /** 文件名称 **/
    private String fileName;
    /** 文件访问服务地址 **/
    private String fileDomain;
    /** 文件访问Url **/
    private String fileUrl;
}
