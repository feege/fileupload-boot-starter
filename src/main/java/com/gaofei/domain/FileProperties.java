package com.gaofei.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author : gaofee
 * @date : 13:26 2021/4/23
 * @码云地址 : https://gitee.com/itgaofee
 */
@Setter
@Getter
@ConfigurationProperties(prefix = "file")
public class FileProperties {

    private String path;
    private String domain;
    private boolean enable;
}
